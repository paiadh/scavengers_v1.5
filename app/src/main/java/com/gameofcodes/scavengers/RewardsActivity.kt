package com.gameofcodes.scavengers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import android.provider.AlarmClock
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_gallerymain.*
import kotlinx.android.synthetic.main.activity_rewards.*
import android.content.DialogInterface
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import android.view.Window.FEATURE_NO_TITLE
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_maps.*

class RewardsActivity : AppCompatActivity() {

    internal var pStatus = 0
    private val handler = Handler()
    internal lateinit var tv: TextView
    val TAG = "Message:"
    var rcid : Int = 0
    // private var txtProgress: TextView? = null
    //var userCurrentScore = 0
    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rewards)

        val btnReward1 = findViewById(R.id.btnReward1) as Button
        val btnReward2 = findViewById(R.id.btnReward2) as Button
        val btnReward3 = findViewById(R.id.btnReward3) as Button
        val btnReward4 = findViewById(R.id.btnReward4) as Button

        val txtProgress = findViewById(R.id.txtProgress) as TextView

        //     btnReward1.isEnabled = false
        // btnReward2.isEnabled = false
        //   btnReward3.isEnabled = false
        //   btnReward4.isEnabled = false

        btnReward1.setAlpha(.5f);
        btnReward2.setAlpha(.5f);
        btnReward3.setAlpha(.5f);
        btnReward4.setAlpha(.5f);

        FirebaseApp.initializeApp(this)
        auth = FirebaseAuth.getInstance()

        retrieveUserHistory()
        getUserPoints()

        val menu = navigation_view2.menu
        val menu_item = menu.getItem(1)
        menu_item.setChecked(true)
        /*
        * Handles the bottom navigation item click
        */
        navigation_view2.setOnNavigationItemSelectedListener { item ->
            var message = ""
            when (item.itemId) {
                R.id.action_home -> {
                    goToHome()
                    message = "Home"
                }
                R.id.action_rewards -> {
                    goToRewards()
                    message = "Rewards"
                }
                R.id.action_gallery -> {
                    goToGallery()
                    message = "Gallery"
                }
                R.id.action_leaderboard -> {
                    goToLeaderboard()
                    message = "Leaderboard"
                }
                R.id.action_profile -> {
                    goToProfile()
                    message = "Profile"
                }
            }
            //Show a message
            Toast.makeText(
                this@RewardsActivity,
                message,
                Toast.LENGTH_SHORT
            ).show()
            return@setOnNavigationItemSelectedListener true
        }

    }




    //Get users points history from firebase
    fun retrieveUserHistory(){
        val fbHistory = db.collection("History")
        //  fbHistory.orderBy("pointsEarned", Query.Direction.DESCENDING).limit(10)

        val userHistoryLocation: ArrayList<String> = ArrayList()
        val userHistoryPoints: ArrayList<String> = ArrayList()
        var userPoints = 0

        fbHistory
            .whereEqualTo("userId", "${auth.uid}")
            .orderBy("date", Query.Direction.DESCENDING)
            .limit(10)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {

                    Log.d(TAG, "${document.id} => ${document.data}")
                    //userPoints = Integer.parseInt(document.data["pointsEarned"].toString())

                    userHistoryLocation.add(document.data["locationName"].toString())
                    userHistoryPoints.add(document.data["pointsEarned"].toString())

                }
                recyclerView.layoutManager = LinearLayoutManager(this)
                recyclerView.adapter = UsersAdapter(userHistoryLocation, userHistoryPoints)

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }


    }


    fun getUserPoints(){

        val docRef = db.collection("Users").document("${auth.uid}")
        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            val source = if (snapshot != null && snapshot.metadata.hasPendingWrites())
                "Local"
            else
                "Server"

            if (snapshot != null && snapshot.exists()) {
                Log.d(TAG, "$source data: ${snapshot.data?.get("currentpoints")}")
                var userCurrentScore = Integer.parseInt(snapshot.data?.get("currentpoints").toString())
//

                checkRewardEligibilty(userCurrentScore)
                loadProgress(userCurrentScore)

            } else {
                Log.d(TAG, "$source data: null")
            }
        }

    }


    fun loadProgress(userCurrentScore : Int){

        println("Score4: "+userCurrentScore)
        pStatus = 0
        var count = 0
        val res = resources
        val drawable = res.getDrawable(R.drawable.circular)
        val mProgress = findViewById<View>(R.id.circularProgressbar) as ProgressBar
        mProgress.progress = 0   // Main Progress
        mProgress.secondaryProgress = 1500 // Secondary Progress
        mProgress.max = 1500 // Maximum Progress
        mProgress.progressDrawable = drawable

        tv = findViewById<View>(R.id.tv) as TextView
        Thread(Runnable {
            // TODO Auto-generated method stub
            while (pStatus < userCurrentScore) {

                pStatus += 1
                handler.post {
                    // TODO Auto-generated method stub
                    mProgress.progress = pStatus
                    tv.text = pStatus.toString()
                }
                try {
                    // Sleep for 200 milliseconds.
                    // Just to display the progress slowly
                    Thread.sleep(4) //thread will take approx 3 seconds to finish
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }
        }).start()
    }

    fun checkRewardEligibilty(userCurrentScore: Int){

        txtProgress.text = "Keep Tapping!"

        if(userCurrentScore >= 1000){
            btnReward1.setAlpha(1f);
            txtProgress.text = "Reward Available!"
        }else{
            btnReward1.setAlpha(0.5f);
        }

        if(userCurrentScore >= 1500){
            btnReward2.setAlpha(1f);
            txtProgress.text = "Rewards Available!"
        }else{
            btnReward2.setAlpha(0.5f);
        }



    }


    fun claimReward1(view: View) {

        val alertDialog = AlertDialog.Builder(this@RewardsActivity)
        val i = Intent(applicationContext, ClaimReward::class.java)

        if(btnReward1.alpha.equals(0.5f)){
            alertDialog.setTitle("Reward Info:")
            alertDialog.setMessage("Free Coffee\n1000 Points")
            alertDialog.show()
        }else{
            i.putExtra("rewardType", 1)
            startActivity(i)
        }


    }

    fun claimReward2(view: View) {
        val alertDialog = AlertDialog.Builder(this@RewardsActivity)
        val i = Intent(applicationContext, ClaimReward::class.java)

        if(btnReward2.alpha.equals(0.5f)){
            alertDialog.setTitle("Reward Info:")
            alertDialog.setMessage("Free Bagel\n1500 Points")
            alertDialog.show()
        }else{
            i.putExtra("rewardType", 2)
            startActivity(i)
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.i(TAG, "onSaveInstanceState1")
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        Log.i(TAG, "onRestoreInstanceState1")
    }

    override fun onResume() {
        super.onResume()
        retrieveUserHistory()
        val circularProgTxt = findViewById(R.id.tv) as TextView
        circularProgTxt.text = "0"
        getUserPoints()
        Log.i(TAG, "onResume1")
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "onPause1")
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "onStop1")
    }

    override fun onRestart() {
        super.onRestart()
        //  getUserPoints()
        Log.i(TAG, "onRestart1")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "onDestroy1")
    }

    fun claimReward3(view: View) {
        val alertDialog = AlertDialog.Builder(this@RewardsActivity)
        alertDialog.setTitle("Reward Info:")
        alertDialog.setMessage("Coming Soon")
        alertDialog.show()
    }
    fun claimReward4(view: View) {
        val alertDialog = AlertDialog.Builder(this@RewardsActivity)
        alertDialog.setTitle("Reward Info:")
        alertDialog.setMessage("Coming Soon")
        alertDialog.show()
    }

    fun goToHome() {
        var message = "Home"
        val intent = Intent(this, MapsActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }

    fun goToRewards() {
        var message = "Rewards"
        val intent = Intent(this, RewardsActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }

    fun goToLeaderboard() {
        var message = "Leaderboard"
        val intent = Intent(this, LeaderboardActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }

    fun goToGallery() {
        var message = "Gallery"
        val intent = Intent(this, ImageActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }

    fun goToProfile() {
        var message = "Profile"
        val intent = Intent(this, ProfileActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
//        intent.putExtra("message", "$userName")
//        Log.d("Printing Uname here: ", "$userName")
        startActivity(intent)
    }

    override fun onBackPressed() {
        val a = Intent(this, MapsActivity::class.java)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }

}