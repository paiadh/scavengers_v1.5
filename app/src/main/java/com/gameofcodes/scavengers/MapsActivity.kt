/**
 * Scavengers
 * Owner: GameOfCodes
 * Revised: Sept 15, 2019
 * Dashboard Screen
 * Developer: Adhrika Pai
 */

@file:Suppress("DEPRECATION")

package com.gameofcodes.scavengers

import android.Manifest
import android.Manifest.*
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.AlarmClock
import android.util.Log
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.appcompat.app.AppCompatActivity

import androidx.core.app.ActivityCompat
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.location.places.ui.PlaceAutocomplete.getPlace
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.activity_maps.navigation_view
import android.app.SearchManager;
import android.view.View
import android.view.WindowManager
import android.widget.RelativeLayout
import android.widget.SearchView;



class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    override fun onMarkerClick(p0: Marker?) = false
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private var visited : Boolean = false

    // Declare a LocationCallback property.
    private lateinit var locationCallback: LocationCallback
    // Declare a LocationRequest property and a location updated state property.
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false

    // Required for getting user's name
    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val REQUEST_CHECK_SETTINGS = 2
        private const val PLACE_PICKER_REQUEST = 3

    }

    private fun startLocationUpdates() {
        /*In startLocationUpdates(), if the ACCESS_FINE_LOCATION
         * permission has not been granted, request it now and return.
         */
        if (ActivityCompat.checkSelfPermission(
                this,
                permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        //If there is permission, request for location updates.
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null /* Looper */
        )
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
        task.addOnFailureListener { e ->
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    e.startResolutionForResult(
                        this@MapsActivity,
                        REQUEST_CHECK_SETTINGS
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    private fun loadPlacePicker() {
        println("Place picker clicked")
        val builder = PlacePicker.IntentBuilder()
        try {
            println("Inside Try-catch")
            startActivityForResult(builder.build(this@MapsActivity), PLACE_PICKER_REQUEST)
        } catch (e: GooglePlayServicesRepairableException) {
            e.printStackTrace()
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        }
    }


    /* Override AppCompatActivity’s onActivityResult() method and start the update
     * request if it has a RESULT_OK result for a REQUEST_CHECK_SETTINGS request.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                locationUpdateState = true
                startLocationUpdates()
            } else if (resultCode == Activity.RESULT_CANCELED) {
                finish()
            }
        }
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                val place = getPlace(this, data)
                placeMarkerOnMap(place.latLng)
            }
        }

    }

    // Override onPause() to stop location update request
    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    // Override onResume() to restart the location update request.
    public override fun onResume() {
        super.onResume()
        if (!locationUpdateState) {
            startLocationUpdates()
        }
    }
    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        startActivity(intent)
    }

    var userName: String = "nu"

    var searchThis : String = "Empty"

    //Retrieving data from Firestore
    var placeName = "Empty"
    var placeDescrition = "Empty"
    var placeAddress = "Empty"
    var placeGeo: GeoPoint = GeoPoint(30.0, 30.0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        auth = FirebaseAuth.getInstance()
        val user = auth.currentUser
        readData {
            userName = "${it}"
        }
        Log.d("User Email is", "${user?.email}")
        Log.d("User id is", "${user?.uid}")

        /* update lastLocation with the new location and update the map with the new location coordinates. */
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                lastLocation = p0.lastLocation
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lastLocation.latitude, lastLocation.longitude), 16f))
            }
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        createLocationRequest()

        val search = findViewById<SearchView>(R.id.searchView) as SearchView
        search.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean{
                searchView.clearFocus()
                searchView.setQuery("" , false)
                Toast.makeText(this@MapsActivity, "$query", Toast.LENGTH_LONG).show()
                searchThis = "$query"

                searchLocation() {
                    Log.d("TAG", it.toString())

                    Toast.makeText(applicationContext, "Found! $placeName at $placeGeo ", Toast.LENGTH_LONG).show()
                    val currentLatLng = LatLng(placeGeo.latitude, placeGeo.longitude)
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 16f))
                    placeMarkerOnMap(currentLatLng)

                    //onMapReady.
                }
                return true

            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })

        val menu = navigation_view.menu
        val menu_item = menu.getItem(0)
        menu_item.isChecked = true
        /*
        * Handles the bottom navigation item click
        */
        navigation_view.setOnNavigationItemSelectedListener { item ->
            var message = ""
            when (item.itemId) {
                R.id.action_home -> {
                    goToHome()
                    message = "Home"
                }
                R.id.action_rewards -> {
                    goToRewards()
                    message = "Rewards"
                }
                R.id.action_gallery -> {
                    goToGallery()
                    message = "Gallery"
                }
                R.id.action_leaderboard -> {
                    goToLeaderboard()
                    message = "Leaderboard"
                }
                R.id.action_profile -> {
                    goToProfile()
                    message = "Profile"
                }
            }
            //Show a message
            Toast.makeText(
                this@MapsActivity,
                message,
                LENGTH_SHORT
            ).show()
            return@setOnNavigationItemSelectedListener true
        }
    }

    /*override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        val manager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchItem = menu?.findItem(R.id.searchView)

        val searchView = searchItem?.actionView as SearchView

        searchView.setSearchableInfo(manager.getSearchableInfo(componentName))
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean{
                searchView.clearFocus()
                searchView.setQuery("" , false)
                searchItem.collapseActionView()
                Toast.makeText(this@MapsActivity, "$query", Toast.LENGTH_LONG).show()
                return true

            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
        return false
    }
*/
    fun goToHome() {
        var message = "Home"
        val intent = Intent(this, MapsActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }

    fun goToRewards() {
        var message = "Rewards"
        val intent = Intent(this, RewardsActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }

    fun goToLeaderboard() {
        var message = "Leaderboard"
        val intent = Intent(this, LeaderboardActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }

    fun goToGallery() {
        var message = "Gallery"
        val intent = Intent(this, ImageActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }

    fun goToProfile() {
        var message = "Profile"
        val intent = Intent(this, ProfileActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        intent.putExtra("message", "$userName")
        Log.d("Printing Uname here: ", "$userName")
        startActivity(intent)
    }

    private fun placeMarkerOnMap(location: LatLng) {
        val markerOptions = MarkerOptions().position(location)
        map.addMarker(markerOptions)
    }

    private fun setUpMap() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_DENIED) {
                val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
                requestPermissions(permissions, LOCATION_PERMISSION_REQUEST_CODE)
            }
            map.isMyLocationEnabled = true
            map.mapType = GoogleMap.MAP_TYPE_TERRAIN
            fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    lastLocation = location
                    val currentLatLng = LatLng(location.latitude, location.longitude)
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 16f))
                }
            }
        }

    }

    @SuppressLint("ResourceType")
    override fun onMapReady(googleMap: GoogleMap) {
        println("Inside MapOnReady")
        map = googleMap
        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMarkerClickListener(this)
        map.mapType = GoogleMap.MAP_TYPE_TERRAIN

        getAllLocation()

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            println("Inside if checking permission maponready")

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
                requestPermissions(permissions, LOCATION_PERMISSION_REQUEST_CODE)
            }
            else {
                setUpMap()
                map.isMyLocationEnabled = true
                //map.setPadding(0,1400,0,0)
                val mapFragment = supportFragmentManager
                    .findFragmentById(R.id.map) as SupportMapFragment

                val locationButton= (mapFragment.view?.findViewById<View>(Integer.parseInt("1"))?.parent as View).findViewById<View>(Integer.parseInt("2"))
                val rlp=locationButton.layoutParams as (RelativeLayout.LayoutParams)
                // position on right bottom
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP,0)
                rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE)
                rlp.setMargins(0,0,30,450)

                val zoombutton= (mapFragment.view?.findViewById<View>(Integer.parseInt("1")))
                val zbut= zoombutton?.layoutParams as (RelativeLayout.LayoutParams)
                zbut.addRule(RelativeLayout.ALIGN_PARENT_TOP,0)
                zbut.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE)
                zbut.setMargins(0,0,30,200)

                //go to map and zoom out showing user and destination location
                fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        lastLocation = location
                        val currentLatLng = LatLng(location.latitude, location.longitude)
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 16f))
                    }
                }
            }
        }

    }

    var locationTagName = ""

    fun addAllLocation(tagLocation: ArrayList<String>, tagCoordinates: ArrayList<GeoPoint>, tagRiddles: ArrayList<String>) {
        var locationName = ""
        for ((index) in tagLocation.withIndex()) {
            val latLng =
                LatLng(tagCoordinates[index].latitude, tagCoordinates[index].longitude)

            map.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .title(tagLocation[index])
                    .snippet("Hint: " + tagRiddles[index])
                    .icon(
                        BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)
                    )
            )
        }
    }

    fun setMarkerColor(tagLocation: ArrayList<String>, tagCoordinates: ArrayList<GeoPoint>, tagRiddles: ArrayList<String>){
        var locationName = ""
        for ((index) in tagLocation.withIndex()) {
            val latLng =
                LatLng(tagCoordinates[index].latitude, tagCoordinates[index].longitude)
            db.collection("History")
                .whereEqualTo("userId", "${auth.uid}")
                .whereEqualTo("locationName", tagLocation[index].toString())
                .get()
                .addOnSuccessListener { documents ->
                    for (document in documents) {
                        locationName = (document.get("locationName").toString())
                        if (locationName.equals(tagLocation[index].toString())) {
                            println("Setting the visited to be true")
                            map.addMarker(
                                MarkerOptions()
                                    .position(latLng)
                                    .title(tagLocation[index])
                                    .snippet("Hint: " + tagRiddles[index])
                                    .icon(
                                        BitmapDescriptorFactory.defaultMarker(
                                            BitmapDescriptorFactory.HUE_AZURE
                                        )
                                    )
                            )
                        }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w("Error", "Error getting setting color of marker.", exception)
                }
        }
    }
    fun getAllLocation(){
        val tagLocation: ArrayList<String> = ArrayList()
        val tagCoordinates: ArrayList<GeoPoint> = ArrayList()
        val tagRiddles: ArrayList<String> = ArrayList()
        db.collection("Tags")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    tagLocation.add(document.data["Place"].toString())
                    tagRiddles.add(document.data["Riddle"].toString())
                    document.getGeoPoint("Location")?.let {
                        tagCoordinates.add(it)
                    }
                }
                addAllLocation(tagLocation, tagCoordinates, tagRiddles)
                Handler().postDelayed(
                    {
                        setMarkerColor(tagLocation, tagCoordinates, tagRiddles)
                    },
                    50 // value in milliseconds
                )

            }
            .addOnFailureListener { exception ->
            }
    }

    fun searchLocation(myCallback : (String) -> Unit){

        var searchString = searchThis

        db.collection("Tags")
            .whereEqualTo("Place", "$searchString")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    println(document.get("Place"))
                    placeName = (document.get("Place")).toString()
                    placeAddress = (document.get("Address")).toString()
                    document.getGeoPoint("Location")?.let {
                        placeGeo = it
                    }
                }
                Log.d("This is Place: " , "$placeName")
                Log.d("All tag info", "$placeName, $placeAddress, $placeDescrition")
                myCallback(placeName)
            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents.", exception)
            }
    }
    //Following code opens connection from database to read username for now.
    //Can read other columns too. Assign (String) to List<String>
    fun readData(myCallback: (String) -> Unit) {
        var currentUName: String
        db.collection("Users").document("${auth.uid}")
            .get()
            .addOnSuccessListener { document ->
                println(document.get("UserName"))
                currentUName = (document.get("UserName")).toString()
                Log.d("This is userName: ", "$currentUName")
                Log.d("Document info", "${document.id} => ${document.data}")
                myCallback(currentUName)
            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents.", exception)
            }
    }
}