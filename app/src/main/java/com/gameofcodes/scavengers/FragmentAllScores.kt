package com.gameofcodes.scavengers

import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.all_scores.*

class FragmentAllScores: Fragment() {
    companion object {
        private lateinit var auth: FirebaseAuth
        val db = FirebaseFirestore.getInstance()
        var currentUserName: String = ""
        fun newInstance(message: String): FragmentAllScores {

            val f = FragmentAllScores()
            val bdl = Bundle(1)
            bdl.putString(EXTRA_MESSAGE, message)
            f.setArguments(bdl)

            return f

        }
        private lateinit var recyclerView: RecyclerView

    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view: View? = inflater.inflate(R.layout.all_scores, container, false)
        recyclerView = view!!.findViewById(R.id.scoresView)
        FirebaseApp.initializeApp(this!!.context!!)
        auth = FirebaseAuth.getInstance()
        readData() {
            currentUserName = "${it}"
            storeScoresHistory()
        }

        return view
    }

    fun storeScoresHistory(){
        val userAvatar: ArrayList<String> = ArrayList()
        val userUsername: ArrayList<String> = ArrayList()
        val userTotalPoints: ArrayList<Int> = ArrayList()

        db.collection("Users")
            .orderBy("totalPoints", Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    userAvatar.add(document.data["Avatar"].toString())
                    userUsername.add(document.data["UserName"].toString())
                    userTotalPoints.add(document.data["totalPoints"].toString().toInt())
                    println(userTotalPoints)
                }
                scoresView.layoutManager = LinearLayoutManager(context)
                scoresView.adapter = ScoresAdapter(userAvatar, userUsername, userTotalPoints, currentUserName)
            }
            .addOnFailureListener { exception ->
            }


    }
    fun readData(myCallback : (String) -> Unit) {
        db.collection("Users").document("${auth.uid}")
            .get()
            .addOnSuccessListener { document ->
                val currentUser = (document.get("UserName")).toString()
                myCallback(currentUser)
            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents.", exception)
            }
    }
}
