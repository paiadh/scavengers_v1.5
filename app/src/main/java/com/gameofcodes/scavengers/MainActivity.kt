package com.gameofcodes.scavengers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import android.content.Intent
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import android.app.ProgressDialog
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseUser

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private var mDelayHandler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(com.gameofcodes.scavengers.R.layout.activity_main)

        //Animation declaration
        val signin1 = AnimationUtils.loadAnimation(this,R.anim.signin1)


        // Email field, password and 2 buttons
        val editEm = findViewById<EditText>(R.id.tEmail)
        val editpa = findViewById<EditText>(R.id.tPassword)
        val btnClick = findViewById<Button>(R.id.btnLogin)
        val btnSignUp = findViewById<Button>(R.id.btnSignUp)
        val forgotPass = findViewById<Button>(R.id.forgotPassword)
        val resetPassText = findViewById<TextView>(R.id.resetPassText)

        //Getting firebase autherization
        FirebaseApp.initializeApp(this)

        auth = FirebaseAuth.getInstance()

        //Start Animation
        editEm.startAnimation(signin1)
        editpa.startAnimation(signin1)
        btnClick.startAnimation(signin1)
        btnSignUp.startAnimation(signin1)
        forgotPass.startAnimation(signin1)


        // Sign in
        btnClick.setOnClickListener {
            Log.d("Sign in", "signInWithEmail:success")
            // Firebase signIn button
            signIn(editEm.text.toString(),editpa.text.toString())
        }
        // Forgot Password
        forgotPass.setOnClickListener{
            if(!editEm.text.toString().isNullOrBlank()){
                auth.sendPasswordResetEmail(editEm.text.toString())
                Toast.makeText(baseContext, "Please check your Email.", Toast.LENGTH_SHORT).show()
            }
            else{
                resetPassText.visibility = View.VISIBLE
            }
        }

        //Sign up
        btnSignUp.setOnClickListener{
            val myIntent = Intent(this, SignUp::class.java)
            this.startActivity(myIntent)
        }


    }
    // onStart. This fun checks if user is already signed in. Then skips login page
    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser

        updateUI(currentUser)
        if(auth.currentUser?.uid != null){
            val myIntent = Intent(this, com.gameofcodes.scavengers.MapsActivity::class.java)
            this.startActivity(myIntent)
        }
        else{
            auth.signOut()
            val currentUser = auth.currentUser?.uid
            Log.d("Signed out!", "Current user is back to:- $currentUser")
        }
    }

    private fun signIn(email: String, password: String) {
        Log.d("Current_SignIn_Email", "signIn:$email")
        // validate here
        val pd = ProgressDialog(this)
        pd.setMessage("Signing In")
        pd.show()

        // Firebase Sign in Fun
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success
                    Log.d("Sign in", "Sign in :success")
                    val user = auth.currentUser
                    updateUI(user)
                    val myIntent = Intent(this, com.gameofcodes.scavengers.MapsActivity::class.java)
                    this.startActivity(myIntent)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Sign in", "Sign in :failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }
                // [START_EXCLUDE]
                if (!task.isSuccessful) {
                    Log.d("Sign in", "Task is not successful")
                }
                pd.hide()
            }
    }
    //Sign up
    private fun signOut() {
        auth.signOut()
        updateUI(null)
    }

    private fun updateUI(user: FirebaseUser?) {
        val pd = ProgressDialog(this)
        pd.setMessage("loading")
        pd.hide()
        if (user != null) {
            Log.d("Log in", "Log in successful")
        } else {
            // Error
            Log.d("Log in", "Log in not successful")
        }
    }
}