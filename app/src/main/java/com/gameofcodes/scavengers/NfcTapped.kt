package com.gameofcodes.scavengers

import android.content.Intent
import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.Ndef
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_nfc_tapped.*
import android.content.IntentFilter
import android.app.PendingIntent

class NfcTapped : AppCompatActivity() {

    var detectedTag: Tag? = null
    private val TIME_OUT = 2000 //Time to launch the another activity
    var tagId = "0"

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        if(NfcAdapter.ACTION_NDEF_DISCOVERED == intent?.action){
            intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
            detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            //val message: List<NdefMessage> = rawMessages.map{it as NdefMessage}
            readFromTag(intent)
            setIntent(intent)
        }
    }
    var txtType: String = "Empty"

    private val mHideHandler = Handler()
    private val mHidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        nfcTxt.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }
    private val mShowPart2Runnable = Runnable {
        // Delayed display of UI elements
        supportActionBar?.show()
        fullscreen_content_controls.visibility = View.VISIBLE
    }
    private var mVisible: Boolean = false
    private val mHideRunnable = Runnable { hide() }

    var readTagFilters: Array<IntentFilter>? = null
    var pendingIntent: PendingIntent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_nfc_tapped)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mVisible = true

        detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG)
        pendingIntent = PendingIntent.getActivity(
            applicationContext, 0,
            Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        val tagDetected = IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED)
        val filter2 = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
        readTagFilters = arrayOf(tagDetected, filter2)

        //Unomment following line to make NFC work
        readFromTag(intent)

        //tagId = "4"

        Handler().postDelayed({
            val i = Intent(this, NfcDetails::class.java)
            i.putExtra("NfcTagId", tagId)
            startActivity(i)
            finish()
        }, TIME_OUT.toLong())
    }

    fun readFromTag(intent: Intent) {
        val ndef = Ndef.get(detectedTag)
        try {
            ndef.connect()
            txtType = ndef.type.toString()
            Log.d("tag info : ", txtType)
            val messages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
            if (messages != null) {
                val ndefMessages = arrayOfNulls<NdefMessage>(messages.size)
                for (i in messages.indices) {
                    ndefMessages[i] = messages[i] as NdefMessage
                    Log.d("In loop $i", ndefMessages[i].toString())
                }
                val record = ndefMessages[0]?.getRecords()?.get(0)
                Log.d("In loop 2: record", "$record")
                val payload = record?.payload
                Log.d("In loop 3: payload", "$payload")
                val text = payload?.let { String(it) }
                Log.d("In loop 4: text", "$text")

                // val record2 = ndefMessages[1]?.getRecords()?.get(1) //Getting id
                val str = "$text"
                val numberOnly = str.replace("[^0-9]".toRegex(), "")
                Log.d("NumberOnly", "$numberOnly")
                tagId = "$numberOnly"
                Log.d("Thisistag", "$tagId")

             //   nfcTxt.setText("Congratulations! You just SCORED!!")
                ndef.close()
            }
        } catch (e: Exception) {
            Toast.makeText(applicationContext, "Cannot Read From Tag.", Toast.LENGTH_LONG).show()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100)
    }

    private fun hide() {
        // Hide UI first
        supportActionBar?.hide()
        fullscreen_content_controls.visibility = View.GONE
        mVisible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable)
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    private fun show() {
        // Show the system bar
        nfcTxt.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        mVisible = true

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable)
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    /**
     * Schedules a call to hide() in [delayMillis], canceling any
     * previously scheduled calls.
     */
    private fun delayedHide(delayMillis: Int) {
        mHideHandler.removeCallbacks(mHideRunnable)
        mHideHandler.postDelayed(mHideRunnable, delayMillis.toLong())
    }

    companion object {

        private val UI_ANIMATION_DELAY = 300
    }
}
