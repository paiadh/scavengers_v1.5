package com.gameofcodes.scavengers

import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_maps.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T




class ImageActivity :AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    internal lateinit var rvImages: RecyclerView
    val db = FirebaseFirestore.getInstance()

    var imageLink: ArrayList<String> = ArrayList()
    var imageLocation: ArrayList<String> = ArrayList()
    var locationsList: ArrayList<String> = ArrayList()

    val imageRef = db.collection("StoredImages")
    val placesRef = db.collection("Tags")
    var selectedItem = String()




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallerymain)

        FirebaseApp.initializeApp(this)
        auth = FirebaseAuth.getInstance()

        locationsList.add(("All Location"))

        val menu = navigation_view.menu
        val menu_item = menu.getItem(2)
        menu_item.setChecked(true)
        /*
        * Handles the bottom navigation item click
        */
        navigation_view.setOnNavigationItemSelectedListener { item ->
            var message = ""
            when (item.itemId) {
                R.id.action_home -> {
                    goToHome()
                    message = "Home"
                }
                R.id.action_rewards -> {
                    goToRewards()
                    message = "Rewards"
                }
                R.id.action_gallery -> {
                    goToGallery()
                    message = "Gallery"
                }
                R.id.action_leaderboard -> {
                    goToLeaderboard()
                    message = "Leaderboard"
                }
                R.id.action_profile -> {
                    goToProfile()
                    message = "Profile"
                }
            }
            //Show a message
            Toast.makeText(
                this@ImageActivity,
                message,
                Toast.LENGTH_SHORT
            ).show()
            return@setOnNavigationItemSelectedListener true
        }

        loadSpinner()


    }

    fun loadSpinner(){

        val spinner : Spinner = findViewById(R.id.cb_location)

        //Getting Places from Firebase
        placesRef
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    locationsList.add(document.data["Place"].toString())
                    Log.e("ERROR", locationsList.toString())

                    // Change Image displayed on ItemClick
                    spinner.onItemSelectedListener = object : OnItemSelectedListener{
                        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                            val selectedItem = parent.getItemAtPosition(position).toString()
                            when {
                                selectedItem.equals("Sheridan College, Davis B Wing") -> retrieveLocation()
                                selectedItem.equals("Niagara Falls") -> retrieveLocation()
                                selectedItem.equals("Square One Mall") -> retrieveLocation()
                                selectedItem.equals("Sheridan College, Trafalgar B Wing") -> retrieveLocation()
                                selectedItem.equals("Sheridan College, HMC A Wing") -> retrieveLocation()
                                else -> retrieveAllPhotos()
                            }
                        }

                        override fun onNothingSelected(parent: AdapterView<*>) {
                        }
                    }
                }

                val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, locationsList)
                spinner.adapter = adapter
            }
            .addOnFailureListener { exception ->
                Log.e("ERROR", "Error getting documents: ", exception)
            }
    }



    fun retrieveLocation(){
        imageLink.clear()
        rvImages = findViewById<View>(R.id.rv_images) as RecyclerView

        val spinner : Spinner = findViewById(R.id.cb_location)
        val selected = spinner.getSelectedItem().toString()

        Log.e("SELECTED", selected)

        imageRef
            .whereEqualTo("locationName", selected)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    imageLink.add(document.data["imageUrl"].toString())
                    imageLocation.add(document.data["locationName"].toString())
                    Log.e("1st", imageLink.toString())
                }
                rvImages.layoutManager = LinearLayoutManager(this)
                rvImages.adapter = ImageAdapter(imageLocation, imageLink)
                rvImages.adapter?.notifyDataSetChanged()
            }
            .addOnFailureListener { exception ->
                Log.e("ERROR", "Error getting documents: ", exception)
            }
    }

    fun retrieveAllPhotos(){
        rvImages = findViewById<View>(R.id.rv_images) as RecyclerView

        imageRef
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    imageLink.add(document.data["imageUrl"].toString())
                    imageLocation.add(document.data["locationName"].toString())
                    Log.e("ALLIMAGES", imageLink.toString())
                }
                rvImages.layoutManager = LinearLayoutManager(this)
                rvImages.adapter = ImageAdapter(imageLocation, imageLink)
            }
            .addOnFailureListener { exception ->
                Log.e("ERROR", "Error getting documents: ", exception)
            }
    }

    //Go back to Maps on back
    override fun onBackPressed() {
        val a = Intent(this, MapsActivity::class.java)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }

    fun goToHome(){
        var message = "Home"
        val intent = Intent(this, MapsActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToRewards(){
        var message = "Rewards"
        val intent = Intent(this, RewardsActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToLeaderboard(){
        var message = "Leaderboard"
        val intent = Intent(this, LeaderboardActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToGallery(){
        var message = "Gallery"
        val intent = Intent(this, ImageActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToProfile(){
        var message = "Profile"
        val intent = Intent(this, ProfileActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        Log.d("Test", "Just checking")
        startActivity(intent)
    }
}
