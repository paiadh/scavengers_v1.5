package com.gameofcodes.scavengers

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_leaderboard.*
import kotlinx.android.synthetic.main.activity_maps.navigation_view
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.all_scores.*


class LeaderboardActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()
    var currentUserName: String = ""
    private lateinit var viewpager: ViewPager
    private lateinit var tabs: TabLayout

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leaderboard)

        FirebaseApp.initializeApp(this)
        auth = FirebaseAuth.getInstance()
        readData() {
            currentUserName = "${it}"
        }
        initViews()
        setupViewPager()
        val menu = navigation_view.menu
        val menu_item = menu.getItem(3)
        menu_item.setChecked(true)
        /*
         * Handles the bottom navigation item click
         */

        navigation_view.setOnNavigationItemSelectedListener { item ->
            var message =""
            when(item.itemId){
                R.id.action_home -> {
                    goToHome()
                    message = "Home"
                }
                R.id.action_rewards -> {
                    goToRewards()
                    message = "Rewards"
                }
                R.id.action_gallery -> {
                    goToGallery()
                    message = "Gallery"
                }
                R.id.action_leaderboard -> {
                    goToLeaderboard()
                    message = "Leaderboard"
                }
                R.id.action_profile -> {
                    goToProfile()
                    message = "Profile"
                }
            }
            //Show a message
            Toast.makeText(this@LeaderboardActivity,
                message,
                Toast.LENGTH_SHORT
            ).show()

            return@setOnNavigationItemSelectedListener true
        }

    }

    fun readData(myCallback : (String) -> Unit) {
        db.collection("Users").document("${auth.uid}")
            .get()
            .addOnSuccessListener { document ->
                val currentUser = (document.get("UserName")).toString()
                myCallback(currentUser)
            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents.", exception)
            }
    }

    override fun onBackPressed() {
        val a = Intent(this, MapsActivity::class.java)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }

    fun goToHome(){
        var message = "Home"
        val intent = Intent(this, MapsActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }

    fun goToRewards(){
        var message = "Rewards"
        val intent = Intent(this, RewardsActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToLeaderboard(){
        var message = "Leaderboard"
        val intent = Intent(this, LeaderboardActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToGallery(){
        var message = "Gallery"
        val intent = Intent(this, ImageActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToProfile(){
        var message = "Profile"
        val intent = Intent(this, ProfileActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }

    private fun initViews() {
        tabs = findViewById(R.id.tabs)
        viewpager = findViewById(R.id.viewpager)
    }

    private fun setupViewPager() {

        val adapter = ViewPageAdapter(getSupportFragmentManager())

        var fragmentAllScores: FragmentAllScores = FragmentAllScores.newInstance("First Fragment")
        var fragmentTopScores: FragmentTopScores = FragmentTopScores.newInstance("Second Fragment")

        adapter.addFragment(fragmentAllScores, "All Scores")
        adapter.addFragment(fragmentTopScores, "Top 3 Scores")

        viewpager!!.adapter = adapter
        tabs!!.setupWithViewPager(viewpager)

    }

}


