package com.gameofcodes.scavengers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.activity_maps.navigation_view
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.choose_profilepic.*

class ProfileActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()

    var locationHistory: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        FirebaseApp.initializeApp(this)
        auth = FirebaseAuth.getInstance()

        val docRef = db.collection("Users").document("${auth.uid}")

        val userName = findViewById<TextView>(R.id.uName)
        val fname = findViewById<TextView>(R.id.txt_fname)
        val lname = findViewById<TextView>(R.id.txt_lname)
        val phone = findViewById<TextView>(R.id.txt_phone)
        val email = findViewById<TextView>(R.id.txt_email)
        val points = findViewById<TextView>(R.id.txt_points)
        val level = findViewById<TextView>(R.id.txt_level)

        val image = findViewById<ImageView>(R.id.iv_avatar)

/*
        val lvlOne : String = "0"
        val lvlTwo : String = "1000"
        val lvlThree : String = "2000"
        val lvlFour : String = "4000"
        val lvlFive : String = "5000"
        val lvlSix : String = "6000"
        val lvlSeven : String = "7000"
        val lvlEight : String = "8000"
        val lvlNine : String = "9000"
        val lvlTen : String = "10000"
*/

        //Following code getting Username value sent from previous page. Message has Username
        val bundle = intent.extras
        val message = bundle!!.getString("message")
        Log.d("Auth.id is", "${auth.uid}")

        //Getting User Information from database
        docRef.get().addOnCompleteListener { task ->
            docRef.addSnapshotListener { snapshot, e ->
                if (snapshot != null && snapshot.exists()) {
                    var dbUsername = (snapshot.data?.get("UserName").toString())
                    var dbFname = (snapshot.data?.get("FirstName").toString())
                    var dbLname = (snapshot.data?.get("LastName").toString())
                    var dbPhone = (snapshot.data?.get("PhoneNum").toString())
                    var dbEmail = (snapshot.data?.get("Email").toString())
                    var dbCurrentPoints = (snapshot.data?.get("currentpoints").toString())
                    var dbTotalPoints  = (snapshot.data?.get("totalPoints").toString())
                    var dbAvatar  = (snapshot.data?.get("Avatar").toString())

                    fname.setText(dbFname)
                    lname.setText(dbLname)
                    phone.setText(dbPhone)
                    email.setText(dbEmail)
                    fname.setText(dbFname)
                    points.setText(dbCurrentPoints)
                    userName.setText(dbUsername)

                    if (dbAvatar == "img1"){
                        image.setBackgroundResource(R.drawable.img1)
                    } else if (dbAvatar == "img2"){
                        image.setBackgroundResource(R.drawable.img2)
                    } else if (dbAvatar == "img3"){
                        image.setBackgroundResource(R.drawable.img3)
                    } else if (dbAvatar == "img4"){
                        image.setBackgroundResource(R.drawable.img4)
                    } else if (dbAvatar == "img5"){
                        image.setBackgroundResource(R.drawable.img5)
                    } else if (dbAvatar == "img6"){
                        image.setBackgroundResource(R.drawable.img6)
                    } else{
                        image.setBackgroundResource(R.drawable.img7)
                    }

                    /*  if (dbTotalPoints == lvlOne && dbTotalPoints < lvlTwo){
                          level.setText("1")
                      } else if (dbTotalPoints > lvlTwo && dbTotalPoints < lvlThree){
                          level.setText("2")
                      } else if (dbTotalPoints > lvlThree && dbTotalPoints < lvlFour){
                          level.setText("3")
                      } else if (dbTotalPoints > lvlFour && dbTotalPoints < lvlFive){
                          level.setText("4")
                      } else if (dbTotalPoints > lvlFive && dbTotalPoints < lvlSix){
                          level.setText("5")
                      } else if (dbTotalPoints > lvlSix && dbTotalPoints < lvlSeven) {
                          level.setText("6")
                      } else if (dbTotalPoints > lvlSeven && dbTotalPoints < lvlEight) {
                          level.setText("7")
                      } else if (dbTotalPoints > lvlEight && dbTotalPoints < lvlNine) {
                          level.setText("8")
                      } else if (dbTotalPoints > lvlNine && dbTotalPoints < lvlTen) {
                          level.setText("9")
                      } else{
                          level.setText("10")
                      }*/

                } else {
                    Log.e("ERROR", "Error getting documents!")
                }
            }
        }

        val menu = navigation_view.menu
        val menu_item = menu.getItem(4)
        menu_item.setChecked(true)
        /*
        * Handles the bottom navigation item click
        */
        navigation_view.setOnNavigationItemSelectedListener { item ->
            var message =""
            when(item.itemId){
                R.id.action_home -> {
                    goToHome()
                    message = "Home"
                }
                R.id.action_rewards -> {
                    goToRewards()
                    message = "Rewards"
                }
                R.id.action_gallery -> {
                    goToGallery()
                    message = "Gallery"
                }
                R.id.action_leaderboard -> {
                    goToLeaderboard()
                    message = "Leaderboard"
                }
                R.id.action_profile -> {
                    goToProfile()
                    message = "Profile"
                }
            }
            //Show a message
            Toast.makeText(this@ProfileActivity,
                message,
                Toast.LENGTH_SHORT
            ).show()
            return@setOnNavigationItemSelectedListener true
        }


        location()
    }

    //Getting numnber of places visited by the user
    fun location(){
        val places = findViewById<TextView>(R.id.txt_places)
        val hisRef = db.collection("History")
        hisRef
            .whereEqualTo("userId", "${auth.uid}")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    locationHistory.add(document.data["locationName"].toString())
                    Log.e("PLACES", locationHistory.toString())
                    Log.e("PLACES", locationHistory.count().toString())
                    places.setText(locationHistory.count().toString())
                }
            }
            .addOnFailureListener { exception ->
                Log.e("ERROR", "Error getting documents: ", exception)
            }
    }

    override fun onBackPressed() {
        val a = Intent(this, MapsActivity::class.java)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }

    fun goToHome(){
        var message = "Home"
        val intent = Intent(this, MapsActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToRewards(){
        var message = "Rewards"
        val intent = Intent(this, RewardsActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToLeaderboard(){
        var message = "Leaderboard"
        val intent = Intent(this, LeaderboardActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToGallery(){
        var message = "Gallery"
        val intent = Intent(this, ImageActivity::class.java).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
    fun goToProfile(){

    }

/*    fun readData(myCallback : (String) -> Unit) {
        Log.d("Print UID", "${auth.uid}")
        db.collection("Users").document("${auth.uid}")
            .get()
            .addOnSuccessListener { document ->
                println(document.get("FirstName"))
                val currentUser = (document.get("FirstName")).toString()
                Log.d("This is FirstName: " , "$currentUser")
                Log.d("This is what you wants", "${document.id} => ${document.data}")
                myCallback(currentUser)
            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents.", exception)
            }
    }*/

    fun logOut(view: View) {
        auth.signOut()
        val myIntent = Intent(this, com.gameofcodes.scavengers.MainActivity::class.java)
        this.startActivity(myIntent)
    }

/*    fun editProfile(view: View) {

        val myIntent = Intent(this, com.gameofcodes.scavengers.SignUp::class.java)
        this.startActivity(myIntent)

    }*/
}
